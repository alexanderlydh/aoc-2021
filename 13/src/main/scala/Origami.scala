object Origami {

  import scala.annotation.tailrec

  case class Fold(axis: String, point: Int)
  case class Coordinate(x: Int, y: Int)

  def performFold(coordinates: Set[Coordinate], fold: Fold): Set[Coordinate] =
    fold.axis match {
      case "x" => foldX(coordinates, fold.point)
      case "y" => foldY(coordinates, fold.point)
    }

  def foldX(coordinates: Set[Coordinate], point: Int): Set[Coordinate] =
    coordinates
      .filter(_.x < point)
      .union(
        coordinates
          .filter(_.x > point)
          .map(c => Coordinate(y = c.y, x = calcFold(c.x, point)))
      )

  def foldY(coordinates: Set[Coordinate], point: Int): Set[Coordinate] =
    coordinates
      .filter(_.y < point)
      .union(
        coordinates
          .filter(_.y > point)
          .map(c => Coordinate(y = calcFold(c.y, point), x = c.x))
      )

  def calcFold(coord: Int, point: Int): Int =
    coord - 2 * (coord - point)

  def maxX(coordinates: Set[Coordinate]): Int =
    coordinates.map(_.x).max
  def maxY(coordinates: Set[Coordinate]): Int =
    coordinates.map(_.y).max

  @tailrec
  def performFolds(
      coordinates: Set[Coordinate],
      folds: Seq[Fold]
  ): Set[Coordinate] =
    folds match {
      case Seq() => coordinates
      case _     => performFolds(performFold(coordinates, folds.head), folds.tail)
    }

  def solve2(coordinates: Set[Coordinate], folds: Seq[Fold]): Unit = {
    val foldedPaper = performFolds(coordinates, folds)
    val paper = Array.ofDim[Char](maxY(foldedPaper) + 1, maxX(foldedPaper) + 1)
    foldedPaper.foreach(c => paper(c.y)(c.x) = '░')
    paper.foreach(r => println(r.toList.mkString("")))
  }
}
