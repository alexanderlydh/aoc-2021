import org.scalatest.funsuite._
import Origami.{Fold, Coordinate}

class OrigamiTest extends AnyFunSuite {

  def readFile(filePath: String): Seq[String] = {
    import scala.io.Source
    val file = Source.fromFile(filePath)
    val content = file.getLines.toList
    file.close()
    content
  }

  val testInputFolds: Seq[Fold] = parseFolds(
    readFile(
      "src/test/scala/input-test-folds.txt"
    )
  )

  val testInputCoords: Seq[Coordinate] = parseCoords(
    readFile(
      "src/test/scala/input-test-coords.txt"
    )
  )

  val inputFolds: Seq[Fold] = parseFolds(
    readFile(
      "src/test/scala/input-folds.txt"
    )
  )

  val inputCoords: Seq[Coordinate] = parseCoords(
    readFile(
      "src/test/scala/input-coords.txt"
    )
  )

  val bigFolds: Seq[Fold] = parseFolds(
    readFile(
      "src/test/scala/input-big-folds.txt"
    )
  )

  val bigCoords: Seq[Coordinate] = parseCoords(
    readFile(
      "src/test/scala/input-big-coords.txt"
    )
  )
  def parseFolds(folds: Seq[String]): Seq[Fold] =
    folds.map(_.split(' ').takeRight(1).head.split('=') match {
      case Array(axis, point) => Fold(axis = axis, point = point.toInt)
    })

  def parseCoords(coords: Seq[String]): Seq[Coordinate] =
    coords.map(_.split(',').map(_.toInt) match {
      case Array(x, y) => Coordinate(x, y)
    })

  test("parse folds") {
    println(testInputFolds)
  }

  test("parse coordinate") {
    println(testInputCoords)
  }

  test("perform one fold") {
    println(
      Origami.performFold(testInputCoords.toSet, testInputFolds.head).size
    )
  }

  test("solve 1") {
    println(Origami.performFold(inputCoords.toSet, inputFolds.head).size)
  }

  test("test solve 2") {
    Origami.solve2(testInputCoords.toSet, testInputFolds)
  }

  test("solve big") {
    Origami.solve2(bigCoords.toSet, bigFolds)
  }

}
