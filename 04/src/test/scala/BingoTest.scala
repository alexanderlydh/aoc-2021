import org.scalatest.funsuite._

import scala.language.implicitConversions
import io.{BufferedSource, Source}

class BingoTest extends AnyFunSuite {
  val testBoardsFile: BufferedSource =
    Source.fromFile("src/test/scala/input-test.txt")

  val testBoards: List[board] = parseBoards(testBoardsFile)
  val testNumbersFile: BufferedSource =
    Source.fromFile("src/test/scala/numbers-test.txt")
  val testNumbers: List[Int] = parseNumbers(testNumbersFile)

  def parseBoards(boards: BufferedSource): List[board] = {
    boards.getLines.toList
      .partition(_ != "")
      ._1
      .grouped(5)
      .map(c => board(c))
      .toList
  }

  def parseNumbers(nums: BufferedSource): List[Int] =
    nums.getLines().toList.head.split(",").map(_.toInt).toList

  val inputBoards: List[board] =
    parseBoards(Source.fromFile("src/test/scala/boards.txt"))

  val inputNumbers: List[Int] = parseNumbers(
    Source.fromFile("src/test/scala/numbers.txt")
  )

  test("parseBoards") {
    println(testBoards)
  }

  test("set of bingos") {
    Bingo.play1(boards = testBoards, numbers = List())
  }

  test("bingo1 with testinput") {
    assert(Bingo.play1(testBoards, testNumbers) == 4512)
  }

  test("bingo2 with testinput") {
    assert(Bingo.play2(testBoards, testNumbers) == 1924)
  }

  test("task 1") {
    println(Bingo.play1(inputBoards, inputNumbers))
  }

  test("task 2") {
    println(Bingo.play2(inputBoards, inputNumbers))
  }

  implicit def ListOfStringToInt(los: List[String]): List[List[Int]] =
    los
      .map(a => a.split(" ").filter(_ != ""))
      .map(_.map(b => b.toInt).toList)
}
