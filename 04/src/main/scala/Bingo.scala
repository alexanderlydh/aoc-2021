import scala.annotation.tailrec

object Bingo {

  @tailrec
  def play1(boards: List[board], numbers: List[Int], drawn: Int = 5): Int = {
    val bingoBoards = boards.filter(b => b.bingo(numbers.take(drawn)))
    bingoBoards.length match {
      case 1 => calcWinnings(bingoBoards.head, numbers.take(drawn))
      case _ => play1(boards, numbers, drawn = drawn + 1)
    }
  }

  @tailrec
  def play2(boards: List[board], numbers: List[Int], drawn: Int = 5): Int = {
    val bingoBoards = boards.filter(b => !b.bingo(numbers.take(drawn)))
    bingoBoards.length match {
      case 0 => calcWinnings(boards.head, numbers.take(drawn))
      case _ => play2(bingoBoards, numbers, drawn = drawn + 1)
    }
  }

  def calcWinnings(board: board, numbers: List[Int]): Int =
    board.misses(numbers).sum * numbers.takeRight(1).head
}
