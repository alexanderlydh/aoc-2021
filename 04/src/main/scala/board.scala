case class board(rows: List[List[Int]]) {
  def setsOfBingos: Set[Set[Int]] =
    (this.rows.map(_.toSet) ++ this.rows.transpose.map(_.toSet)).toSet

  def toSet: Set[Int] = this.rows.map(_.toSet).reduce(_ ++ _)

  def hits(numbers: List[Int]): List[Int] =
    (this.toSet intersect numbers.toSet).toList

  def misses(numbers: List[Int]): List[Int] =
    (this.toSet -- this.hits(numbers)).toList

  def bingo(numbers: List[Int]): Boolean =
    this.setsOfBingos.map(_ subsetOf numbers.toSet).toList.reduce(_ || _)
}

object board {
  def apply(rows: List[List[Int]]): board = new board(rows = rows)
}
