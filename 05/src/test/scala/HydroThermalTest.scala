import org.scalatest.funsuite._

import io.{BufferedSource, Source}

class HydroThermalTest extends AnyFunSuite {
  val testFile: BufferedSource =
    Source.fromFile("src/test/scala/input-test.txt")
  val inputFile: BufferedSource =
    Source.fromFile("src/test/scala/input.txt")

  def parseInput(file: BufferedSource): Seq[Line] =
    file.getLines.toList
      .map(l =>
        l.split(" -> ") match {
          case Array(c1, c2) => Line(from = Coordinate(c1), to = Coordinate(c2))
        }
      )

  val testInstructions: Seq[Line] = parseInput(testFile)

  test("parsing") {
    println(testInstructions)
    testInstructions.map(c => println(c.dense))
  }

  test("apply test data") {
    val matrix = HydroThermal.applyLines(testInstructions)
    assert(HydroThermal.countBiggerThan(matrix) == 12)
  }

  val inputInstructions: Seq[Line] = parseInput(inputFile)
  test("apply input data") {
    val matrix = HydroThermal.applyLines(inputInstructions)
    println(HydroThermal.countBiggerThan(matrix))
  }

}
