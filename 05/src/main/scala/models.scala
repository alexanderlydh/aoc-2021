import scala.math.abs
case class Coordinate(x: Int, y: Int)

object Coordinate {
  def apply(coords: String): Coordinate = {
    val splitCoord = coords.split(",")
    new Coordinate(x = splitCoord(0).toInt, y = splitCoord(1).toInt)
  }
}

case class Line(from: Coordinate, to: Coordinate) {
  def dense: List[Coordinate] =
    if (is45) {
      dense45
    } else {
      denseStraight
    }

  def denseStraight: List[Coordinate] =
    posRange(from.x, to.x)
      .flatMap(x => posRange(from.y, to.y).map(y => Coordinate(x, y)).toList)
      .toList

  def dense45: List[Coordinate] =
    negRange(from.x, to.x)
      .zip(negRange(from.y, to.y))
      .map(c => Coordinate(c._1, c._2))
      .toList

  def not45: Boolean = !is45
  def isStraight: Boolean = isVertical || isHorizontal || is45
  def is45: Boolean = abs(from.x - to.x) == abs(from.y - to.y)
  def isVertical: Boolean = from.x == to.x
  def isHorizontal: Boolean = from.y == to.y

  def negRange(from: Int, to: Int): Range =
    if (from < to) {
      from to to
    } else {
      from to to by -1
    }

  def posRange(from: Int, to: Int): Range =
    if (from < to) {
      from to to
    } else {
      to to from
    }
}
