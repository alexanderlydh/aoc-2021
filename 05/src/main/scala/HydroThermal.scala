object HydroThermal {
  type Matrix = Map[Coordinate, Int]
  def applyLines(lines: Seq[Line]): Matrix = {
    lines
      .filter(_.isStraight)
      .flatMap(l => l.dense)
      .groupBy(identity)
      .transform((_, v) => v.length)
  }

  def countBiggerThan(matrix: Matrix, limit: Int = 1): Int =
    matrix.count { case (_, v) => v > limit }

}
