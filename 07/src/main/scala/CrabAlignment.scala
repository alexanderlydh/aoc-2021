import scala.math.abs
object CrabAlignment {
  type CostFun = (Int, Int) => Int
  def align(crabs: List[Int], alignPosition: Int, costFun: CostFun): Int =
    crabs.map(p => costFun(p, alignPosition)).sum
  def findBestAlign(crabs: List[Int], costFun: CostFun): Int =
    (0 to crabs.max).map(p => align(crabs, p, costFun)).min

  def costFun1(p: Int, d: Int): Int = abs(p - d)
  def costFun2(p: Int, d: Int): Int = (1 to costFun1(p, d)).sum

}
