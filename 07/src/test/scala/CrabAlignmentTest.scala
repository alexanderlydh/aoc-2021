import org.scalatest.funsuite._

import io.{BufferedSource, Source}

class CrabAlignmentTest extends AnyFunSuite {
  val testInput = List(16, 1, 2, 0, 4, 2, 7, 1, 2, 14)
  val inputFile: BufferedSource = Source.fromFile("src/test/scala/input.txt")
  val inputCrabs: List[Int] =
    inputFile.getLines.toList.head.split(",").map(_.toInt).toList

  test("test input") {
    assert(CrabAlignment.align(testInput, 2, CrabAlignment.costFun1) == 37)
  }

  test("test solve 1") {
    assert(CrabAlignment.findBestAlign(testInput, CrabAlignment.costFun1) == 37)
  }

  test("solve 1") {
    println(CrabAlignment.findBestAlign(inputCrabs, CrabAlignment.costFun1))
  }

  test("test solve 2") {
    assert(
      CrabAlignment.findBestAlign(testInput, CrabAlignment.costFun2) == 168
    )
  }

  test("solve 2") {
    println(CrabAlignment.findBestAlign(inputCrabs, CrabAlignment.costFun2))
  }
}
