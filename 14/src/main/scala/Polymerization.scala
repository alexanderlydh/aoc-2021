object Polymerization {

  import scala.annotation.tailrec

  type InsertionRules = Map[String, String]
  type countMap = Map[String, Long]
  case class Polymer(chain: String)

  var molecules: Map[String, Seq[String]] = Map[String, Seq[String]]()

  def solve1(
      polymer: Polymer,
      insertionRules: InsertionRules,
      steps: Int
  ): Int = {
    val expandedPolymer = expandPolymerRec(polymer, insertionRules, steps)
    val counts =
      expandedPolymer.chain.groupBy(identity).transform((_, v) => v.length)
    counts.maxBy { case (k, v) => v }._2 - counts.minBy { case (k, v) => v }._2
  }

  def solve2(
      polymer: Polymer,
      insertionRules: InsertionRules,
      steps: Int
  ): Long = {
    val counts = countLetters(polymer, insertionRules, steps)
    counts.maxBy { case (_, v) => v }._2 - counts.minBy { case (_, v) => v }._2
  }

  def countLetters(
      polymer: Polymer,
      insertionRules: InsertionRules,
      steps: Int
  ): Map[Char, Long] =
    getOfCreatePolymerRec(getPolymerCount(polymer), insertionRules, steps)
      .map(s => s._1.map(c => (c, s._2)))
      .flatten
      .groupBy(t => t._1)
      .transform((_, v) => (v.map(_._2).sum + 1) / 2)

  @tailrec
  def getOfCreatePolymerRec(
      counts: countMap,
      insertionRules: InsertionRules,
      steps: Int
  ): countMap = {
    steps match {
      case 0 => counts
      case _ =>
        val newCounts =
          counts
            .map(c =>
              getOrCreatePolymer(c._1, insertionRules).map(m => (m, c._2))
            )
            .flatten
            .groupBy(t => t._1)
            .transform((_, v) => v.map(_._2).sum)
        getOfCreatePolymerRec(newCounts, insertionRules, steps - 1)
    }
  }

  def getOrCreatePolymer(
      m: String,
      insertionRules: InsertionRules
  ): Seq[String] =
    molecules.get(m) match {
      case Some(p) => p
      case None =>
        molecules += (m -> (m.head + insertionRules(m) + m.tail)
          .sliding(2)
          .toSeq)
        molecules(m)
    }

  def getPolymerCount(polymer: Polymer): Map[String, Long] =
    polymer.chain
      .sliding(2)
      .toList
      .groupBy(identity)
      .transform((_, v) => v.length.toLong)

  @tailrec
  def expandPolymerRec(
      polymer: Polymer,
      insertionRules: InsertionRules,
      steps: Int
  ): Polymer = {
    steps match {
      case 0 => polymer
      case _ =>
        expandPolymerRec(
          expandPolymer(polymer, insertionRules),
          insertionRules,
          steps - 1
        )
    }
  }

  def expandPolymer(polymer: Polymer, insertionRules: InsertionRules): Polymer =
    Polymer(chain =
      polymer.chain
        .sliding(2)
        .map(p => p.head + insertionRules(p) + p.tail)
        .reduceLeft((a, e) => a + e.tail)
    )

}
