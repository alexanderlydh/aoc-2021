import org.scalatest.funsuite._
import Polymerization.{Polymer, InsertionRules}

class PolymerizationTest extends AnyFunSuite {

  def readFile(filePath: String): Seq[String] = {
    import scala.io.Source
    val file = Source.fromFile(filePath)
    val content = file.getLines.toList
    file.close()
    content
  }

  def parseInput(fileContent: Seq[String]): (Polymer, InsertionRules) = {
    val polymer = Polymer(chain = fileContent.head)
    val insertionRules = fileContent.tail
      .flatMap(_.split(" -> ") match {
        case Array(from, to) => Some((from, to))
        case _               => None
      })
      .toMap
    (polymer, insertionRules)
  }

  val testInput: (Polymer, InsertionRules) = parseInput(
    readFile("src/test/scala/input-test.txt")
  )
  val input: (Polymer, InsertionRules) = parseInput(
    readFile("src/test/scala/input.txt")
  )

  test("parse input") {
    println(testInput)
  }

  test("test ep") {
    println(
      Polymerization
        .expandPolymer(testInput._1, testInput._2)
    )
  }

  test("test expand polymer") {
    assert(
      Polymerization
        .expandPolymer(testInput._1, testInput._2) == Polymer(chain = "NCNBCHB")
    )
  }

  test("rec expand polymer") {
    assert(
      Polymerization
        .expandPolymerRec(testInput._1, testInput._2, 2) == Polymer(chain =
        "NBCCNBBBCBHCB"
      )
    )
  }

  test("rec expand polymer big") {
    assert(
      Polymerization
        .expandPolymerRec(testInput._1, testInput._2, 4) == Polymer(chain =
        "NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB"
      )
    )
  }

  test("solve 1") {
    println(Polymerization.solve1(input._1, input._2, steps = 10))
  }

  test("check 2 with 1") {
    val someMap =
      Polymerization.getPolymerCount(Polymer(chain = "NBCCNBBBCBHCB"))
    assert(
      Polymerization.getOfCreatePolymerRec(
        Polymerization.getPolymerCount(testInput._1),
        testInput._2,
        steps = 2
      ) == someMap
    )
  }

  test("check 2 with 1 step 3") {
    val someMap = Polymerization.getPolymerCount(
      Polymer(chain = "NBBBCNCCNBBNBNBBCHBHHBCHB")
    )
    assert(
      Polymerization.getOfCreatePolymerRec(
        Polymerization.getPolymerCount(testInput._1),
        testInput._2,
        steps = 3
      ) == someMap
    )
  }

  test("check 2 with 1 step 4") {
    val chain = "NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB"
    val someMap = Polymerization.getPolymerCount(Polymer(chain = chain))
    val countedLetters = chain.groupBy(identity).transform((_, v) => v.length)
    assert(
      Polymerization.getOfCreatePolymerRec(
        Polymerization.getPolymerCount(testInput._1),
        testInput._2,
        steps = 4
      ) == someMap
    )
  }

  test("test letter count") {
    val chain = "NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB"
    val countedLetters = chain.groupBy(identity).transform((_, v) => v.length)
    assert(
      Polymerization
        .countLetters(testInput._1, testInput._2, steps = 4) == countedLetters
    )
  }

  test("test solve 2") {
    assert(
      Polymerization
        .solve2(testInput._1, testInput._2, steps = 40) == 2188189693529L
    )
  }

  test("test solve 2 small") {
    println(Polymerization.solve2(testInput._1, testInput._2, steps = 2))
  }

  test("solve 2") {
    println(Polymerization.solve2(input._1, input._2, steps = 40))
  }
}
