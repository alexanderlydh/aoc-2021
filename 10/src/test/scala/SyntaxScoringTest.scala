import org.scalatest.funsuite._

import scala.io.Source

class SyntaxScoringTest extends AnyFunSuite {
  def readFile(filePath: String): Seq[String] = {
    val file = Source.fromFile(filePath)
    val content = file.getLines.toList
    file.close()
    content
  }

  val testInput: Seq[String] = readFile("src/test/scala/input-test.txt")
  val input: Seq[String] = readFile("src/test/scala/input.txt")

  test("parse input") {
    println(testInput)
  }

  test("test solve 1") {
    assert(SyntaxScoring.solve1(testInput) == 26397)
  }

  test("solve 1") {
    println(SyntaxScoring.solve1(input))
  }

  test("test solve 2") {
    assert(SyntaxScoring.solve2(testInput) == 288957)
  }

  test("solve 2") {
    println(SyntaxScoring.solve2(input))
  }
}
