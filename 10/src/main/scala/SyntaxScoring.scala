import scala.collection.mutable.ArrayBuffer

object SyntaxScoring {

  val closingPairs = Map(')' -> '(', ']' -> '[', '}' -> '{', '>' -> '<')
  val allPairs = Map(
    ')' -> '(',
    ']' -> '[',
    '}' -> '{',
    '>' -> '<',
    '(' -> ')',
    '[' -> ']',
    '{' -> '}',
    '<' -> '>'
  )
  val scores1 = Map(')' -> 3, ']' -> 57, '}' -> 1197, '>' -> 25137)
  val scores2 = Map('(' -> 1L, '[' -> 2L, '{' -> 3L, '<' -> 4L)

  def isOpening(c: Char): Boolean = "([{<" contains c

  def isClosing(c: Char): Boolean = ")]}>" contains c

  def isMatchingPair(o: Char, c: Char): Boolean = o == allPairs(c)

  def solve2(instructions: Seq[String]): Long =
    instructions
      .map(findUnmatched)
      .filterNot(isCorrupt)
      .map(_._1)
      .map(calcScore2)
      .sorted
      .lift(instructions.length / 2)
      .get

  def calcScore2(unmatchedBraces: Seq[Char]): Long =
    unmatchedBraces.reverse
      .map(scores2)
      .reduce((ts, lc) => 5 * ts + lc)

  def isCorrupt(i: (Seq[Char], Seq[Char])): Boolean =
    i._2.nonEmpty

  def solve1(instructions: Seq[String]): Int =
    instructions
      .map(findUnmatched)
      .filter(_._2.nonEmpty)
      .map(um => scores1(um._2.head))
      .sum

  def findUnmatched(string: String): (Seq[Char], Seq[Char]) = {
    var leftBrackets = new ArrayBuffer[Char]
    val unmatchedRightBrackets = new ArrayBuffer[Char]

    for (c <- string) {
      if (isOpening(c))
        leftBrackets.append(c)
      else if (isClosing(c)) {
        if (closingPairs(c) != leftBrackets.last)
          unmatchedRightBrackets.append(c)
        leftBrackets = leftBrackets.dropRight(1)
      }
    }
    (leftBrackets.toSeq, unmatchedRightBrackets.toSeq)
  }

}
