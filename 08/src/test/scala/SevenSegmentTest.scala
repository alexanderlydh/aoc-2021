import org.scalatest.funsuite._
import io.Source

class SevenSegmentTest extends AnyFunSuite {

  val testInputRaw = List(
    "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"
  )

  def parseInput(input: Seq[String]): Seq[(Seq[String], Seq[String])] =
    input.map(_.split('|').map(_.trim).map(_.strip) match {
      case Array(a, b) => (treatString(a), treatString(b))
    })

  def treatString(s: String): Seq[String] = s.split(" ").map(_.sorted)

  val testInput: Seq[(Seq[String], Seq[String])] = parseInput(testInputRaw)

  val testFile: Seq[String] =
    Source.fromFile("src/test/scala/input-test.txt").getLines.toList

  val inputFile: Seq[String] =
    Source.fromFile("src/test/scala/input.txt").getLines.toList

  val input: Seq[(Seq[String], Seq[String])] = parseInput(inputFile)

  val testInputBig: Seq[(Seq[String], Seq[String])] = parseInput(testFile)

  test("parse test") {
    println(parseInput(testInputRaw))
  }

  test("test solve1") {
    SevenSegment.solve1(testInputBig) == 26
  }

  test("solve 1") {
    println(SevenSegment.solve1(input))
  }

  test("test applyKey") {
    assert(
      testInput
        .map(c =>
          SevenSegment
            .applyKey(output = c._2, key = SevenSegment.dechiffer(c._1))
        )
        .head == 5353
    )

  }

  test("test 2") {
    assert(SevenSegment.solve2(testInputBig) == 61229)
  }

  test("solve 2") {
    println(SevenSegment.solve2(input))
  }
}
