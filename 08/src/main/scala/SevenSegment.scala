object SevenSegment {
  val SegmentMap: Map[String, String] = Map(
    "abcefg" -> "0",
    "cf" -> "1",
    "acdeg" -> "2",
    "acdfg" -> "3",
    "bcdf" -> "4",
    "abdfg" -> "5",
    "abdefg" -> "6",
    "acf" -> "7",
    "abcdefg" -> "8",
    "abcdfg" -> "9"
  )

  type Key = Map[String, Char]

  def findEasyNumbers(input: Seq[String]): Seq[(Int, String)] = {
    input.flatMap(e =>
      e.length match {
        case 2 => Some((1, e))
        case 3 => Some((7, e))
        case 4 => Some((4, e))
        case 7 => Some((8, e))
        case _ => None
      }
    )
  }

  def solve1(input: Seq[(Seq[String], Seq[String])]): Int =
    input.map(i => findEasyNumbers(i._2).length).sum

  def solve2(input: Seq[(Seq[String], Seq[String])]): Int =
    input.map(i => applyKey(output = i._2, key = dechiffer(i._1))).sum

  def applyKey(output: Seq[String], key: Key): Int =
    output
      .map(s => s.map(c => key(c.toString)).sorted)
      .map(SegmentMap(_))
      .reduce(_ + _)
      .toInt

  def getLetter(gc: Map[Char, Int], size: Int): String =
    gc.filter(e => e._2 == size).toList.head._1.toString

  def dechiffer(chiffer: Seq[String]): Key = {
    val sortedChiffer = chiffer.map(_.sorted)
    val easyMap = findEasyNumbers(sortedChiffer).toMap

    val groupedChiffer = sortedChiffer
      .reduce(_ + _)
      .groupBy(identity)
      .view
      .mapValues(_.length)
      .toMap

    val a = easyMap(7).diff(easyMap(1))
    val b = getLetter(groupedChiffer, 6)

    val f = getLetter(groupedChiffer, 9)
    val c = easyMap(1).diff(f)
    val d = easyMap(4).diff(b + c + f)

    val e = getLetter(groupedChiffer, 4)
    val g = easyMap(8).diff(a + b + c + d + e + f)

    Map(
      a -> 'a',
      b -> 'b',
      c -> 'c',
      d -> 'd',
      e -> 'e',
      f -> 'f',
      g -> 'g'
    )

  }
}
