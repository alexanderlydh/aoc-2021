import org.scalatest.funsuite._
import io.Source

class BinaryDiagnosticsTest extends AnyFunSuite {
  val diagnosticsReport: List[String] = List(
    "00100",
    "11110",
    "10110",
    "10111",
    "10101",
    "01111",
    "00111",
    "11100",
    "10000",
    "11001",
    "00010",
    "01010"
  )
  test("example diagnose") {
    assert(BinaryDiagnostics.diagnose1(diagnosticsReport) == 198)
  }
  val rotatedList = List(
    "011110011100",
    "010001010101",
    "111111110000",
    "011101100011",
    "000111100100"
  )

  test("calcGamma") {
    assert(BinaryDiagnostics.calcGamma(rotatedList) == 22)
  }

  test("calcEpsilon") {
    assert(BinaryDiagnostics.calcEpsilon(rotatedList) == 9)
  }

  test("Task 1") {
    val bufferedSource = Source.fromFile("src/test/scala/input.txt")
    val binaries = bufferedSource.getLines.toList
    println(BinaryDiagnostics.diagnose1(binaries))
  }

  test("testOxygen") {
    assert(BinaryDiagnostics.calcOxygenRating(diagnosticsReport) == 23)
  }

  test("testCo2") {
    assert(BinaryDiagnostics.calcCo2Rating(diagnosticsReport) == 10)
  }

  test("Task 2") {
    val bufferedSource = Source.fromFile("src/test/scala/input.txt")
    val binaries = bufferedSource.getLines.toList
    println(BinaryDiagnostics.diagnose2(binaries))
  }
}
