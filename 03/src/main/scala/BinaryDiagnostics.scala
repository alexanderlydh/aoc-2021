import scala.annotation.tailrec
import scala.collection.immutable.ListMap

object BinaryDiagnostics {
  def diagnose1(diagnosticReport: List[String]): Int = {
    val rotBin = rotateBinaries(diagnosticReport)
    calcGamma(rotBin) * calcEpsilon(rotBin)
  }

  def calcGamma(binaries: List[String]): Int =
    parseInt(binaries, mostCommonChar)

  def mostCommonChar(s: String): Char =
    ListMap(groupByLength(s).toSeq.sortWith(_._1 > _._1): _*).maxBy(_._2)._1

  def groupByLength(s: String): Map[Char, Int] =
    s.groupBy(identity).transform((_, v) => v.length)

  def parseInt(binaries: List[String], parser: String => Char): Int =
    Integer.parseInt(binaries.map(parser).foldLeft("")((x, y) => x + y), 2)

  def calcEpsilon(binaries: List[String]): Int =
    parseInt(binaries, leastCommonChar)

  def leastCommonChar(s: String): Char =
    ListMap(groupByLength(s).toSeq.sortBy(_._1): _*).minBy(_._2)._1

  def rotateBinaries(binaries: List[String]): List[String] =
    binaries.transpose.map(_.mkString)

  def diagnose2(diagnosticReport: List[String]): Int =
    calcCo2Rating(diagnosticReport) * calcOxygenRating(diagnosticReport)

  def calcOxygenRating(binaries: List[String]): Int =
    filterBinaries(binaries, mostCommonChar, 0)

  def calcCo2Rating(binaries: List[String]): Int =
    filterBinaries(binaries, leastCommonChar, 0)

  @tailrec
  def filterBinaries(
      binaries: List[String],
      binaryFilter: String => Char,
      index: Int
  ): Int =
    binaries.length match {
      case 1 => Integer.parseInt(binaries.head, 2)
      case _ =>
        filterBinaries(
          doFilter(binaries, binaryFilter, index),
          binaryFilter,
          index + 1
        )
    }

  def doFilter(b: List[String], f: String => Char, index: Int): List[String] = {
    val filterChar = f(rotateBinaries(b)(index))
    b.filter(s => s(index) == filterChar)
  }
}
