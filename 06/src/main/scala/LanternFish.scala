object LanternFish {

  def count(fish: List[Int]): Array[Long] =
    (0 to 8).map(i => fish.count(_ == i).longValue).toArray

  def rotate(l: Array[Long]): Array[Long] = l.drop(1) ++ l.take(1)

  def solve(fish: List[Int], days: Int): Long = {
    var d = days
    var rotatedFish = count(fish)
    while (d > 0) {
      rotatedFish = rotate(rotatedFish)
      rotatedFish(6) += rotatedFish(8)
      d -= 1
    }
    rotatedFish.sum
  }

}
