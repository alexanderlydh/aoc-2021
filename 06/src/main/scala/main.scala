import scala.io.{BufferedSource, Source}

object main extends App {

  val inputFile: BufferedSource = Source.fromFile("src/test/scala/input.txt")
  val laternfish: List[Int] =
    inputFile.getLines.toList.head.split(",").map(_.toInt).toList

  println(LanternFish.solve(laternfish, days = 256))
}
