import org.scalatest.funsuite._

import io.{BufferedSource, Source}

class LanternFishTest extends AnyFunSuite {
  val testInput = List(3, 4, 3, 1, 2)

  val inputFile: BufferedSource = Source.fromFile("src/test/scala/input.txt")
  val laternfish: List[Int] =
    inputFile.getLines.toList.head.split(",").map(_.toInt).toList

  test("solve test") {
    assert(LanternFish.solve(testInput, days = 80) == 5934)
  }

  test("solve test big") {
    assert(LanternFish.solve(testInput, days = 256) == 26984457539L)
  }

  test("task 1") {

    println(LanternFish.solve(laternfish, days = 256))
  }

  test("solve 1") {

    LanternFish.solve(laternfish, days = 10)
  }

}
