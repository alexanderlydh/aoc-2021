import org.scalatest.funsuite._
import PassagePathing.CaveMap

class PassagePathingTest extends AnyFunSuite {

  def readFile(filePath: String): Seq[String] = {
    import scala.io.Source
    val file = Source.fromFile(filePath)
    val content = file.getLines.toList
    file.close()
    content
  }

  val testInput: CaveMap = parseInput(readFile("src/test/scala/input-test.txt"))
  val testInput19: CaveMap = parseInput(
    readFile("src/test/scala/input-test-19.txt")
  )
  val testInput226: CaveMap = parseInput(
    readFile("src/test/scala/input-test-226.txt")
  )
  val input: CaveMap = parseInput(readFile("src/test/scala/input.txt"))

  def parseInput(input: Seq[String]): CaveMap =
    input
      .flatMap(_.split('-') match { case Array(f, t) => Seq((f, t), (t, f)) })
      .groupBy(_._1)
      .transform((_, v) => v.map(_._2).toSet)

  test("parseInput") {
    println(testInput)
  }

  test("caveStep") {
    println(
      PassagePathing
        .caveStep(testInput, Seq("start"), PassagePathing.visitCave1)
    )
  }

  test("test solve 1") {
    assert(PassagePathing.solve1(testInput) == 10)
  }

  test("test solve 1 19") {
    assert(PassagePathing.solve1(testInput19) == 19)
  }

  test("test solve 1 226") {
    assert(PassagePathing.solve1(testInput226) == 226)
  }

  test("Solve 1") {
    println(PassagePathing.solve1(input))
  }

  test("test solve 2") {
    assert(PassagePathing.solve2(testInput) == 36)
  }

  test("test solve 2 19") {
    assert(PassagePathing.solve2(testInput19) == 103)
  }

  test("test solve 2 226") {
    assert(PassagePathing.solve2(testInput19) == 3509)
  }

  test("Solve 2") {
    println(PassagePathing.solve2(input))
  }
}
