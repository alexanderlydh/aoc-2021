object PassagePathing {
  type CaveMap = Map[Cave, Set[Cave]]
  type Cave = String

  def solve1(caveSystem: CaveMap): Int =
    caveStep(caveSystem, Seq("start"), visitCave1).flatten.length

  def solve2(caveSystem: CaveMap): Int =
    caveStep(caveSystem, Seq("start"), visitCave2).flatten.length

  def caveStep(
      caveSystem: CaveMap,
      visitedCaves: Seq[Cave],
      visitCaveFun: (CaveMap, Cave, Seq[Cave]) => CaveMap
  ): Seq[Option[Seq[Cave]]] =
    visitedCaves.takeRight(1).head match {
      case "end" => Seq(Some(visitedCaves))
      case currentCave =>
        val updatedCaveSystem =
          visitCaveFun(caveSystem, currentCave, visitedCaves)
        updatedCaveSystem(currentCave).toSeq match {
          case Seq() => Seq(None)
          case nextCaves =>
            nextCaves
              .filter(_ != "start")
              .flatMap(c =>
                caveStep(updatedCaveSystem, visitedCaves :+ c, visitCaveFun)
              )
        }
    }

  def visitCave1(
      caveMap: CaveMap,
      cave: Cave,
      visitedCaves: Seq[Cave]
  ): CaveMap =
    if (isSmall(cave)) {
      caveMap.transform((_, v) => v.diff(Set(cave)))
    } else {
      caveMap
    }

  def visitCave2(
      caveMap: CaveMap,
      cave: Cave,
      visitedCaves: Seq[Cave]
  ): CaveMap =
    if (isSmall(cave) && smallVisitedTwice(visitedCaves)) {
      caveMap.transform((_, v) => v.diff(visitedCaves.filter(isSmall).toSet))
    } else {
      caveMap
    }

  def smallVisitedTwice(visitedCaves: Seq[Cave]): Boolean =
    visitedCaves
      .filter(isSmall)
      .groupBy(identity)
      .map(v => v._2.length)
      .exists(_ > 1)

  def isSmall(cave: Cave): Boolean =
    cave
      .map(_.isLower)
      .reduce(_ && _)

}
