import org.scalatest.funsuite._
import Dumbo.EnergyLevels

class Dumbotest extends AnyFunSuite {

  def readFile(filePath: String): Seq[String] = {
    import scala.io.Source
    val file = Source.fromFile(filePath)
    val content = file.getLines.toList
    file.close()
    content
  }

  def parseInput(input: Seq[String]): EnergyLevels =
    input.map(_.toSeq.map(_.asDigit))

  val testInputRaw: Seq[String] = readFile("src/test/scala/input-test.txt")
  val inputRaw: Seq[String] = readFile("src/test/scala/input.txt")

  val testInput: EnergyLevels = parseInput(testInputRaw)
  val input: EnergyLevels = parseInput(inputRaw)

  test("parse input") {
    println(parseInput(testInputRaw).mkString("Array(", ", ", ")"))
  }

  test("test solve 1") {
    assert(Dumbo.solve1(testInput) == 1656)
  }

  test("test flashes left") {
    println(Dumbo.flashesInEnergyLevels(testInput))
  }

  test("solve 1") {
    println(Dumbo.solve1(input))
  }

  test("test solve 2") {
    assert(Dumbo.solve2(testInput) == 288957)
  }

  test("solve 2") {
    println(Dumbo.solve2(input))
  }
}
