object Dumbo {

  import scala.annotation.tailrec

  type EnergyLevels = Seq[Seq[Int]]
  type Coordinate = (Int, Int)

  def solve1(energyLevels: EnergyLevels): Int = flashes(energyLevels, 100).sum

  def solve2(energyLevels: EnergyLevels): Int = flashes2(energyLevels, 100)

  @tailrec
  def flashes2(
      energyLevels: EnergyLevels,
      numOfFlashes: Int,
      step: Int = 0
  ): Int = {
    val flashesLeft = flashesInEnergyLevels(energyLevels)
    val flashesLeftLen = flashesLeft.toList.length

    val resetEnergyLevels = resetFlashed(energyLevels)
    val elStep = resetEnergyLevels.map(incByOn1)
    val flashesLeftStep = flashesInEnergyLevels(elStep)

    if (flashesLeftLen == numOfFlashes) {
      step
    } else {
      flashes2(
        flashStep(elStep, flashesLeftStep, Set()),
        numOfFlashes,
        step + 1
      )
    }
  }

  def flashes(energyLevels: EnergyLevels, steps: Int): Seq[Int] = {
    val flashesLeft = flashesInEnergyLevels(energyLevels)
    val flashesLeftLen = Seq(flashesLeft.toList.length)

    val resetEnergyLevels = resetFlashed(energyLevels)
    val elStep = resetEnergyLevels.map(incByOn1)
    val flashesLeftStep = flashesInEnergyLevels(elStep)

    println("Step: ", steps, flashesLeftLen)
    steps match {
      case 0 => flashesLeftLen
      case _ =>
        flashesLeftLen ++ flashes(
          flashStep(elStep, flashesLeftStep, Set()),
          steps - 1
        )
    }
  }

  @tailrec
  def flashStep(
      energyLevels: EnergyLevels,
      flashesLeft: Set[Coordinate],
      flashesThisStep: Set[Coordinate]
  ): EnergyLevels = {
    val toFlash = flashesLeft.diff(flashesThisStep)
    val flashedEnergyLevels =
      toFlash.foldLeft(energyLevels)((a, c) => flashAndUpdateNeighbors(a, c))
    flashesInEnergyLevels(flashedEnergyLevels)
      .diff(flashesThisStep)
      .toSeq match {
      case Seq() => flashedEnergyLevels
      case newFlashesLeft =>
        flashStep(
          flashedEnergyLevels,
          newFlashesLeft.toSet,
          toFlash.union(flashesThisStep)
        )
    }

  }

  def flashAndUpdateNeighbors(
      energyLevels: EnergyLevels,
      c: Coordinate
  ): EnergyLevels =
    getNeightbors(c, energyLevels).foldLeft(energyLevels)((a, c) => flash(a, c))

  def printEnergyLevels(energyLevels: EnergyLevels): EnergyLevels = {
    energyLevels.foreach(l =>
      println(
        l.map(_.toString)
          .foldLeft("")((a, c) => a + " " + { if (c.toInt > 9) "*" else c })
      )
    )
    println()
    energyLevels
  }

  def flash(energyLevels: EnergyLevels, c: Coordinate): EnergyLevels = {
    energyLevels
      .updated(
        c._1,
        energyLevels(c._1)
          .updated(c._2, energyLevels(c._1)(c._2) + 1)
      )
  }

  def resetFlashed(energyLevels: EnergyLevels): EnergyLevels =
    energyLevels.map(r => r.map { c => if (c > 9) 0 else c })

  def incByOn1(l: Seq[Int]): Seq[Int] =
    l.map(_ + 1)

  def flashesInEnergyLevels(energyLevels: EnergyLevels): Set[Coordinate] =
    (for {
      r <- energyLevels.indices
      c <- energyLevels.head.indices
      coord <- hasFlash(energyLevels(r)(c), (r, c))
    } yield coord).toSet

  def hasFlash(energyLevel: Int, coordinate: Coordinate): Option[Coordinate] =
    if (energyLevel > 9)
      Some(coordinate)
    else
      None

  def getNeightbors(
      coordinate: Coordinate,
      energyLevels: EnergyLevels
  ): Seq[Coordinate] =
    for {
      x <- 0.max(coordinate._1 - 1) to (coordinate._1 + 1)
        .min(energyLevels.indices.size - 1)
      y <- 0.max(coordinate._2 - 1) to (coordinate._2 + 1)
        .min(energyLevels.head.indices.size - 1)
    } yield (x, y)

}
