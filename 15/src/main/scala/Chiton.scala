
object Chiton {
  type Coordinate = (Int, Int)
  type CaveMap = Seq[Seq[Int]]

  var cheapestPath: Map[Coordinate, Double] = Map[Coordinate, Double]()
  var cameFrom: Map[Coordinate, Coordinate] = Map[Coordinate, Coordinate]()

  def solve2(caveMap: CaveMap): Int =
    solve1(nXnCaveMap(caveMap))

  def nXnCaveMap(caveMap: CaveMap, n: Int = 5): CaveMap =
    (0 until n)
      .map(x =>
        (0 until n)
          .map(y => incCaveMapNTimes(caveMap, n = x + y))
          .reduceLeft((c1, c2) => cavesSidebySide(c1, c2))
      )
      .reduceLeft((c1, c2) => cavesStacked(c1, c2))

  def cavesSidebySide(c1: CaveMap, c2: CaveMap): CaveMap =
    c1.zip(c2).map(c => c._1 ++ c._2)

  def cavesStacked(c1: CaveMap, c2: CaveMap): CaveMap =
    c1 ++ c2

  def incCaveMapNTimes(caveMap: CaveMap, n: Int = 1): CaveMap = {
    var incCave = caveMap
    var nIter = n
    while (nIter > 0) {
      incCave = incCaveMap(incCave)
      nIter = nIter - 1
    }
    incCave
  }

  def incCaveMap(caveMap: CaveMap): CaveMap =
    caveMap.map(r => r.map(c => if (c == 9) 1 else c + 1))

  def solve1(caveMap: CaveMap): Int =
    aStar(caveMap).get
      .map(c => caveMap(c._1)(c._2))
      .tail
      .sum

  def aStar(caveMap: CaveMap): Option[Seq[Coordinate]] = {
    val target = (caveMap.indices.max, caveMap.head.indices.max)
    val startCoordinate = (0, 0)
    var openSet = Set(startCoordinate)

    cheapestPath += (startCoordinate -> 0)

    while (openSet.nonEmpty) {
      val current = getBestGuess(openSet)
      // printCaveMap(caveMap, reconstructPath(current).toSet)
      if (current == target) {
        return Some(reconstructPath(current))
      }

      openSet = openSet - current
      for (n <- getNeighbors(current, target)) {
        val tScore =
          cheapestPath.getOrElse(current, Double.MaxValue) + calcHeuristic(
            n,
            caveMap
          )
        if (tScore < cheapestPath.getOrElse(n, Double.MaxValue)) {
          cameFrom += (n -> current)
          cheapestPath += (n -> tScore)
          openSet = openSet.union(Set(n))
        }
      }
    }
    None
  }

  def printCaveMap(caveMap: CaveMap, path: Set[Coordinate] = Set()): Unit = {
    (0 to caveMap.indices.max).foreach(x =>
      println(
        (0 to caveMap.head.indices.max)
          .map(y => if (path contains (x, y)) "*" else caveMap(x)(y).toString)
          .mkString(" ")
      )
    )
    println()
  }

  def reconstructPath(current: Coordinate): Seq[Coordinate] = {
    var curr = current
    var path = Seq(current)
    while (cameFrom.contains(curr)) {
      curr = cameFrom(curr)
      path = path :+ curr
    }
    path.reverse
  }

  def getBestGuess(openCoordinates: Set[Coordinate]): Coordinate =
    openCoordinates
      .map(oc => (oc, cheapestPath.getOrElse(oc, Double.MaxValue)))
      .minBy(_._2)
      ._1

  def calcHeuristic(coordinate: Coordinate, caveMap: CaveMap): Double =
    getRiskLevel(coordinate, caveMap).getOrElse(Double.MaxValue)

  def getRiskLevel(coordinate: Coordinate, caveMap: CaveMap): Option[Double] =
    for {
      row <- caveMap.lift(coordinate._1)
      riskLevel <- row.lift(coordinate._2)
    } yield riskLevel.toDouble

  def getNeighbors(coord: Coordinate, target: Coordinate) =
    Set(
      (0.max(coord._1 - 1), coord._2),
      (target._1.min(coord._1 + 1), coord._2),
      (coord._1, 0.max(coord._2 - 1)),
      (coord._1, target._2.min(coord._2 + 1))
    )

}
