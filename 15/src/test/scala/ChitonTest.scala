import org.scalatest.funsuite._
import io.Source

class ChitonTest extends AnyFunSuite {
  def readFile(filePath: String): Seq[String] = {
    val file = Source.fromFile(filePath)
    val content = file.getLines.toList
    file.close()
    content
  }

  def parseInput(input: Seq[String]): Seq[Seq[Int]] =
    input.map(_.toSeq.map(_.asDigit))

  val testInput: Seq[Seq[Int]] = parseInput(
    readFile("src/test/scala/input-test.txt")
  )
  val input: Seq[Seq[Int]] = parseInput(readFile("src/test/scala/input.txt"))

  test("parse input") {
    println(testInput)
  }

  test("test solve 1") {
    assert(Chiton.solve1(testInput) == 40)
  }

  test("solve 1") {
    println(Chiton.solve1(input))
  }

  test("inc map") {
    Chiton.printCaveMap(Chiton.incCaveMap(testInput))
  }

  test("inc map 2 times") {
    Chiton.printCaveMap(Chiton.incCaveMapNTimes(testInput, 2))
  }

  test("5x5 map square") {
    Chiton.printCaveMap(Chiton.nXnCaveMap(testInput), Set())
  }

  test("merge cavemap side by side") {
    Chiton.printCaveMap(Chiton.cavesSidebySide(testInput, testInput))
  }

  test("merge cavemap stacked") {
    Chiton.printCaveMap(Chiton.cavesStacked(testInput, testInput))
  }

  test("test solve 2") {
    assert(Chiton.solve2(testInput) == 315)
  }

  test("solve 2") {
    println(Chiton.solve2(input))
  }
}
