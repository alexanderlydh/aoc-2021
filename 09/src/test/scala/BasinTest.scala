import org.scalatest.funsuite._

import io.Source

class BasinTest extends AnyFunSuite {
  val testInputFile: Seq[String] = readFile("src/test/scala/input-test.txt")
  val inputFile: Seq[String] = readFile("src/test/scala/input.txt")

  def readFile(filePath: String): Seq[String] = {
    val file = Source.fromFile(filePath)
    val content = file.getLines.toList
    file.close()
    content
  }

  def parseInput(input: Seq[String]): Seq[Seq[Int]] =
    input.map(_.toCharArray.map(_.toString.toInt).toList).toList

  val testHeightMap: Seq[Seq[Int]] = parseInput(testInputFile)
  val inputHeightMap: Seq[Seq[Int]] = parseInput(inputFile)

  test("parse file") {
    println(testHeightMap.mkString("Array(", ", ", ")"))
  }

  test("find basins") {
    assert(Basin.findLowSpots(testHeightMap) == Seq(1, 0, 5, 5))
  }

  test("test solve 1") {
    assert(Basin.solve1(testHeightMap) == 15)
  }

  test("solve 1") {
    println(Basin.solve1(inputHeightMap))
  }

  test("test solve 2") {
    assert(Basin.solve2(testHeightMap) == 1134)
  }

  test("solve 2") {
    println(Basin.solve2(inputHeightMap))
  }
}
