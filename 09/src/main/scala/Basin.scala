object Basin {

  import scala.annotation.tailrec

  type HeightMap = Seq[Seq[Int]]
  type Coordinate = (Int, Int)

  def solve1(heightMap: HeightMap): Int =
    findLowSpots(heightMap).map(_._1 + 1).sum

  def solve2(heightMap: HeightMap): Int =
    findLowSpots(heightMap)
      .map(c => findBasin(heightMap, Set((c._2, c._3)), Set((c._2, c._3))))
      .map(_.size)
      .sorted
      .takeRight(3)
      .product

  @tailrec
  def findBasin(
      heightMap: HeightMap,
      foundNeighbors: Set[Coordinate],
      newNeighbors: Set[Coordinate]
  ): Set[Coordinate] =
    newNeighbors
      .flatMap(getNeighbors)
      .map(n => (n, getHeight(heightMap, n).getOrElse(9)))
      .filter(_._2 != 9)
      .map(_._1)
      .diff(foundNeighbors) match {
      case s if s.nonEmpty => findBasin(heightMap, foundNeighbors.union(s), s)
      case _               => foundNeighbors
    }

  def findLowSpots(heightMap: HeightMap): Seq[(Int, Int, Int)] =
    for {
      r <- heightMap.indices
      c <- heightMap.head.indices
      ls <- checkLowSpot(heightMap, (r, c))
    } yield (ls, r, c)

  def checkLowSpot(heightMap: HeightMap, coord: Coordinate): Option[Int] = {
    val height = getHeight(heightMap, coord).getOrElse(9)

    getNeighbors(coord)
      .map(n => getHeight(heightMap, n).getOrElse(9))
      .count(_ > height) match {
      case 4 => Some(height)
      case _ => None
    }
  }

  def getHeight(heightMap: HeightMap, coordinate: Coordinate): Option[Int] =
    for {
      row <- heightMap.lift(coordinate._1)
      height <- row.lift(coordinate._2)
    } yield height

  def getNeighbors(coord: Coordinate): Seq[Coordinate] =
    List(
      (coord._1 - 1, coord._2),
      (coord._1 + 1, coord._2),
      (coord._1, coord._2 - 1),
      (coord._1, coord._2 + 1)
    )

}
